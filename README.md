# CTA Data Viz

This repo is a small project to help visualize CTA bus data that is publicly available at the CTA's website.
It contains two smaller projects. One that is a Kotlin REST API to serve specific data and the other is a ReactJS app to vizualize the data using Google Maps.


## Requirments

1. Docker 17.03.0-ce or up
2. Java 8 JDK
3. NodeJS 8
4. npm 5
5. ReactJS 16

## Getting Started

1. clone repo
2. cd /path/to/repo/cta-api
3. ./gradlew clean build
4. cd ../
5. docker-compose build; docker-compose up -d
6. cd ../cta-viz
7. npm start
8. view in browser at localhost:3000

## Running the Tests

1. cd /path/to/repo/cta-api
2. ./gradelw clean test

