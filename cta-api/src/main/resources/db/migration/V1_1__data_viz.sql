CREATE SCHEMA cta;
CREATE TABLE cta.boardings_weekday_avg_08_12 (stop_id INT, on_street TEXT, cross_street TEXT, routes TEXT[], boardings DECIMAL, alightings DECIMAL, month_beginning DATE, daytype TEXT, location POINT);

CREATE OR REPLACE VIEW cta.stops AS
	SELECT stop_id,
		on_street,
		cross_street,
		boardings,
		alightings,
		month_beginning,
		daytype,
		location,
		UNNEST(routes) as route
	FROM cta.boardings_weekday_avg_08_12;

CREATE OR REPLACE FUNCTION cta.get_stop_most_routes()
	RETURNS TABLE (stop_id INT, location POINT) AS $$
	BEGIN
		RETURN QUERY
			SELECT t.stop_id, t.location
			FROM cta.stops t
			WHERE t.stop_id = (SELECT s.stop_id
											 FROM cta.stops s
											 GROUP BY s.stop_id
											 ORDER BY COUNT(route) DESC LIMIT 1)
			LIMIT 1;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION cta.get_boardings(size INT DEFAULT 0)
  RETURNS TABLE (stop_id INT, on_street TEXT, cross_street TEXT, routes TEXT, boardings DECIMAL, alightings DECIMAL, month_beginning DATE, daytype TEXT, location POINT) AS $$

  BEGIN
    CREATE TEMP TABLE tmp_boardings ON COMMIT DROP AS
    SELECT t.stop_id, t.on_street, t.cross_street, array_to_string(t.routes, ',', '*') AS routes, t.boardings, t.alightings, t.month_beginning, t.daytype, t.location
                   FROM  cta.boardings_weekday_avg_08_12 t;
	IF size = 0 THEN
		RETURN QUERY SELECT * FROM tmp_boardings;
	ELSE
		RETURN QUERY SELECT * FROM tmp_boardings LIMIT size;
    END IF;
  END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION cta.get_stop_count_by_route(route TEXT DEFAULT '')
	RETURNS TABLE (route_name TEXT, stop_count INT) AS $$
	BEGIN
		CREATE TEMP TABLE tmp_routes ON COMMIT DROP AS
		SELECT t.route as route_name, COUNT(t.stop_id)::INT AS stop_count
		FROM cta.stops t
		GROUP BY route_name
		ORDER BY stop_count DESC;
	CASE route
	WHEN '' THEN
		RETURN QUERY SELECT r.route_name, r.stop_count FROM tmp_routes r;
	ELSE
		RETURN QUERY SELECT r.route_name, r.stop_count FROM tmp_routes r WHERE r.route_name = route;
	END CASE;
	END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION cta.get_route_stops(route TEXT DEFAULT '')
	RETURNS TABLE (stop_id INT, on_street TEXT, cross_street TEXT, boardings DECIMAL, location POINT) AS $$
BEGIN
	RETURN QUERY SELECT t.stop_id, t.on_street, t.cross_street, t.boardings, t.location
	FROM cta.boardings_weekday_avg_08_12 t
	WHERE route = ANY(routes)
	ORDER BY stop_id ASC;
END
$$ LANGUAGE plpgsql;
