package com.implauzable.cta.routes


import org.http4k.routing.routes
import org.http4k.core.Method.GET
import org.http4k.routing.bind

val app_routes = routes(
        "/health" bind GET to healthHandler,
        "/all/{limit}" bind GET to allStopsHandler,
        "/routes/{routeNum}" bind GET to stopsByRoute,
        "/stops/most" bind GET to stopOnMostRoutes
)