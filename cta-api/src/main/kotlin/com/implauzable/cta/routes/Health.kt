package com.implauzable.cta.routes

import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import com.implauzable.cta.database.DBAccess


val healthHandler = {_: Request ->
    val serverInfo = DBAccess().checkConnection()
    val responseHealth = if(serverInfo.count() > 0) Status.OK else Status.INTERNAL_SERVER_ERROR
    Response(responseHealth).body(resultsToJson(serverInfo))}