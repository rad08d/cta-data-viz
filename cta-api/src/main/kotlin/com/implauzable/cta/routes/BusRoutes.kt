package com.implauzable.cta.routes

import com.implauzable.cta.database.DBAccess
import org.http4k.core.Response
import org.http4k.core.Request
import org.http4k.routing.path
import org.http4k.core.Status
import org.slf4j.LoggerFactory.getLogger

private val log = getLogger("BusRoutesRoute")

private fun processRequest(req: Request, process: (Request) -> List<Map<String, Any>>): Response {
    return try{
        buildResponse(process.invoke(req), Status.OK)
    } catch(ex: Exception) {
        log.error("Can not process request for route ${req.uri}.", ex)
        buildResponse(listOf<Map<String, Any>>(hashMapOf("Error" to "There has been an issue processing your request")), Status.INTERNAL_SERVER_ERROR)
    }
}


private fun processAllStopsRequest(req: Request): Response {
    val param = req.path("limit")
        return try {
            val limit = param!!.toInt()
            buildResponse(DBAccess().getAll(limit), Status.OK)
        } catch (ex: Exception) {
            log.error("An incorrect argument was passed to the endpoint.", ex)
            buildResponse(listOf<Map<String, Any>>(hashMapOf("Bad Request" to "A bad value was passed to the limit parameter")), Status.BAD_REQUEST)
        }
    }

private fun processStopsByRouteRequest(req: Request): List<Map<String, Any>> {
    val route = req.path("routeNum").toString()
    return if (route.equals("longest")) DBAccess().getLongestRoute() else DBAccess().getStopsByRoute(route)
}

private fun processStopMostRoutes(req: Request): List<Map<String, Any>> {
    return DBAccess().getStopWithMostRoutes()
}

val stopOnMostRoutes = {req: Request -> processRequest(req, ::processStopMostRoutes)}
val allStopsHandler = {req: Request ->  processAllStopsRequest(req)}
val stopsByRoute = {req: Request ->  processRequest(req, ::processStopsByRouteRequest)}