package com.implauzable.cta.routes


import com.google.gson.GsonBuilder
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.core.with
import org.http4k.core.Body
import org.http4k.core.ContentType
import org.http4k.lens.string

private val gson = GsonBuilder().setPrettyPrinting().create()

fun resultsToJson(results: List<Map<String, Any>>): String {
    return gson.toJson(results)
}

fun buildResponse(results: List<Map<String, Any>>, status: Status): Response {
    return Response(status).with(Body.string(ContentType.APPLICATION_JSON).toLens() of resultsToJson(results))
}

