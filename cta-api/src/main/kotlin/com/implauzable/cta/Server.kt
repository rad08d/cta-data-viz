package com.implauzable.cta

import com.implauzable.cta.database.runFlyway
import com.implauzable.cta.routes.app_routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import org.slf4j.LoggerFactory.getLogger

private val log = getLogger("CtaApiServer")


fun main(args: Array<String>) {
    val config = Config
    var count = 0
    var migrate = false
    // Try migration
    while (count < 5) {
        try {
            runFlyway("db")
            runFlyway("db/seeding")
            migrate = true
            break
        } catch(ex: Exception){
            log.error("Flyway failed. Trying again.")
            Thread.sleep(2000)
            count++
        }
    }
    when(migrate){
        true -> {
            val app = app_routes
            app.asServer(Jetty(config.httpPort)).start()
            log.info("Using HTTP httpPort of: ${config.httpPort}")
        }
        false -> {
            log.error("Can not run database migration and seeding.")
            System.exit(1)
        }
    }

}