package com.implauzable.cta.database

import org.flywaydb.core.Flyway
import org.slf4j.LoggerFactory.getLogger

private val log = getLogger("CtaApiFlyway")

fun runFlyway(path: String) {
    log.info("Attempting flyway migration on $path")
    val flyway = Flyway()
    flyway.dataSource = DBAccess().getDs()
    flyway.setLocations(path)
    flyway.migrate()
}