package com.implauzable.cta.database

import com.implauzable.cta.Config
import org.postgresql.ds.PGSimpleDataSource
import java.sql.Connection
import java.sql.ResultSet
import javax.sql.DataSource
import org.slf4j.LoggerFactory.getLogger


class DBAccess {
    private val log = getLogger(javaClass)

    private val config = Config
    private val dbUrl = config.dbUrl
    private val dbPort = config.dbPort
    private val dbUser = config.dbUser
    private val dbPassword = config.dbPassword
    private val dbName = config.dbName


    fun getStopWithMostRoutes(): List<Map<String, Any>> {
        return runSqlSelect("SELECT * FROM cta.get_stop_most_routes();")
    }

    fun getLongestRoute(): List<Map<String, Any>> {
        val routeNum = getRouteMostStops()
        log.info("Checking route number:  ${routeNum}")
        return getStopsByRoute(routeNum)
    }

    fun getRouteMostStops(): String {
        val allRoutes = runSqlSelect("SELECT * FROM cta.get_stop_count_by_route();")
        return allRoutes[0]["route_name"].toString()
    }

    fun getStopsByRoute(route: String=""): List<Map<String, Any>> {
        return runSqlSelect("SELECT * FROM cta.get_route_stops(?);", route)
    }

    fun getAll(limit: Int=0): List<Map<String, Any>> {
        return runSqlSelect("SELECT * FROM cta.get_boardings(?);", limit)
    }

    fun checkConnection(): List<Map<String, Any>> {
        return runSqlSelect("SELECT VERSION();")
    }

    private fun resultToMap(results: ResultSet): List<Map<String, Any>> {
        val metaData = results.metaData
        val headers = mutableListOf<String>()
        val rows = mutableListOf<Map<String, Any>>()
        for (i in 1..metaData.columnCount) {
            headers.add(i-1, metaData.getColumnLabel(i))
        }
        var count = 0
        while (results.next()) {
            val row = mutableMapOf<String, Any>()
            for (i in 1..metaData.columnCount){
                row.put(headers[i-1], results.getObject(i))
            }
            rows.add(count, row)
            count++
        }
        return rows
    }

    private fun runSqlSelect(sqlStatement: String, vararg args: Any): List<Map<String, Any>> {
        getConnection().use{ conn ->
            val statement = conn.prepareStatement(sqlStatement)
            var count = 1
            for(arg in args){
                statement.setObject(count, arg)
                count++
            }
            val results = resultToMap(statement.executeQuery())
            conn.close()
            return results
        }
    }

    fun getConnection(): Connection {
        val ds = getDs()
        return ds.connection
    }

    fun getDs(): DataSource {
        val ds = PGSimpleDataSource()
        ds.serverName = dbUrl
        ds.databaseName = dbName
        ds.user = dbUser
        ds.password = dbPassword
        ds.portNumber = dbPort
        return ds
    }
}

class DBAccessException(override var message:String): Exception(message)