package com.implauzable.cta

import org.slf4j.LoggerFactory.getLogger


object Config {

    private const val envHttpPort = "HTTP_PORT"
    private const val defaultHttpPort = 8080


    private const val envDbUrl = "DB_URL"
    private const val defaultDbUrl = "localhost"

    private const val envDbPort = "DB_PORT"
    private const val defaultDbPort = 5432

    private const val envDbUser = "DB_USER"
    private const val defaultDbUser = "data_viz"

    private const val envDbPassword = "DB_PASSWORD"
    private const val defaultDbPassword = "data"

    private const val envDbName = "DB_NAME"
    private const val defaulDbName = "bus_routes"

    val httpPort = setInt(envHttpPort, defaultHttpPort)
    val dbUrl = setString(envDbUrl, defaultDbUrl)
    val dbPort = setInt(envDbPort, defaultDbPort)
    val dbUser = setString(envDbUser, defaultDbUser)
    val dbPassword = setString(envDbPassword, defaultDbPassword)
    val dbName = setString(envDbName, defaulDbName)

    private val log = getLogger(this.javaClass.name)

    private fun setInt(envName: String, defaultPort: Int): Int {
        val env = System.getenv(envName)
        return when (env.isNullOrBlank()) {
            true -> defaultPort
            false -> try {
                env.toInt()
            } catch (numEx: NumberFormatException) {
                log.error("There is a bad value provided for the $envName. $env cannot be converted to an integer. Defaulting to $defaultPort")
                return defaultPort
            }
        }
    }

    private fun setString(envName: String, defaultString: String): String {
        val env = System.getenv(envName)
        return when (env.isNullOrBlank()) {
            true -> defaultString
            false -> env
        }
    }
}