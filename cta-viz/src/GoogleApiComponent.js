import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';



export class MapContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stops: []
        }
    }

    componentDidMount() {

        fetch(`/routes/${this.props.match.params.routeNum}`)
            .then( response => response.json())
            .then(data => {this.setState({stops: data})});
    }

    render() {
        return (
            <div>
                <Map google={this.props.google}
                     zoom={12}
                     initialCenter={{
                    lat: 41.881832,
                    lng: -87.623177
                }}>
                    {this.state.stops.map((route) => {
                        return(
                            <Marker
                                name={route.on_street}
                                position={{lat: route.location.x, lng: route.location.y }} />
                        );

                    })}
                </Map>
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ("AIzaSyAR7Yxon1zBd5hlbO4vCvs6oDEZd_n8l3Y")
})(MapContainer)