import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import GoogleApiWrapper from './GoogleApiComponent';
import PopularStopComponent from './PopularStopComponent'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div>
              <Router>
                <div>
                  <ul>
                    <li>
                      <Link to="/routes/longest">Bus Routes</Link>
                    </li>
                  <li>
                      <Link to="/stops/most">Stops</Link>
                  </li>
                  </ul>
                  <hr />
                  <Route path="/routes/:routeNum" component={GoogleApiWrapper} />
                  <Route path="/stops/most" component={PopularStopComponent} />
                </div>
              </Router>
        </div>
      </div>
    );
  }
}

export default App;
