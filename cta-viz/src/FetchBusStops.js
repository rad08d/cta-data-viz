import React, { Component } from 'react';

class FetchBusStopsJson extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stops: []
        }
    }

    componentDidMount() {

        fetch('/all/3')
            .then( response => response.json())
            .then(data => {this.setState({stops: data}); console.log(data)});
    }
    render() {
        return (
            <div className="FetchBusStopsJson">
                {this.state.stops.map((route) => {
                    return(
                        <div>
                            <h1>route: {route.routes}</h1>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default FetchBusStopsJson;